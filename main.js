import fs from 'fs';

import { detectJs, detectPy, detectPhp, detectRb } from './detect.js';
import { messages } from './messages/generate.js';
import { specs } from './specs/generate.js';

if (process.argv.length < 4) {
    console.log('Not enough arguments (mode and path to an enolib implementation required)');
    process.exit(1);
}

const modeArg = process.argv[2];
if (modeArg !== 'messages' && modeArg !== 'specs') {
    console.log(`Invalid first argument (mode) '${modeArg}' (may only be 'messages' or 'specs')`);
    process.exit(1);
}

const pathArg = process.argv[3];
if (!fs.existsSync(pathArg)) {
    console.log(`Supplied path ${pathArg} does not exist`);
    process.exit(1);
}

const options = {
    notice: `THIS FILE IS AUTO-GENERATED (Please submit permanent changes to https://codeberg.org/simonrepp/enolib-common)`,
    repoPath: pathArg
};

if (detectJs(options.repoPath)) {
    options.language = 'js';
} else if (detectPy(options.repoPath)) {
    options.language = 'py';
} else if (detectPhp(options.repoPath)) {
    options.language = 'php';
} else if (detectRb(options.repoPath)) {
    options.language = 'rb';
} else {
    console.log(`The path ${pathArg} does not seem to be pointing to a recognized enolib implementation`);
    process.exit(1);
}

if (modeArg === 'messages') {
    messages(options);
} else /* if (modeArg === 'specs') */ {
    specs(options);
}
