import fs from 'fs';
import path from 'path';

export function detectJs(repoPath) {
    const manifestPath = path.join(repoPath, 'package.json');

    if (!fs.existsSync(manifestPath))
        return false;
        
    const manifest = fs.readFileSync(manifestPath, 'utf-8');
    return /^\s*"name"\s*:\s*"enolib"\s*,?\s*$/m.test(manifest);
}

export function detectPy(repoPath) {
    const manifestPath = path.join(repoPath, 'setup.py');

    if (!fs.existsSync(manifestPath))
        return false;
        
    const manifest = fs.readFileSync(manifestPath, 'utf-8');
    return /^\s*name\s*=\s*["']enolib["']\s*,?\s*$/m.test(manifest);
}

export function detectPhp(repoPath) {
    const manifestPath = path.join(repoPath, 'composer.json');

    if (!fs.existsSync(manifestPath))
        return false;

    const manifest = fs.readFileSync(manifestPath, 'utf-8');
    return /^\s*"name"\s*:\s*"eno-lang\/enolib"\s*,?\s*$/m.test(manifest);
}

export function detectRb(repoPath) {
    const manifestPath = path.join(repoPath, 'enolib.gemspec');

    if (!fs.existsSync(manifestPath))
        return false;

    const manifest = fs.readFileSync(manifestPath, 'utf-8');
    return /^\s*spec\.name\s*=\s*["']enolib["']\s*$/m.test(manifest);
}