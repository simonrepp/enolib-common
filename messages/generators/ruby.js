import fs from 'fs';
import fsExtra from 'fs-extra';
import path from 'path';

import { interpolatify } from '../../utilities.js';

const upperCaseInitial = string => string.replace(/^./, initial => initial.toUpperCase());

export function ruby(options) {
    const directory = path.join(options.repoPath, 'lib/enolib');
    const defaultMessages = path.join(directory, 'messages.rb');
    
    const messageFunction = message => {
        let translation = message.translation
        
        if (message.arguments) {
            for (const argument of message.arguments) {
                translation = translation.replace(new RegExp(`{${argument}}`, 'g'), `#{${argument}}`);
            }
            
            return `def self.${message.name}(${message.arguments.join(', ')}) "${translation}" end`;
        } else {
            return `${message.name.toUpperCase()} = '${translation.replace(/'/g, "\\'")}'`;
        }
    };
    
    for (const [locale, messages] of Object.entries(options.locales)) {
        const code = interpolatify`
            # frozen_string_literal: true
            
            # ${options.notice}
            
            module Enolib
              module Messages
                ${messages.map(messageFunction).join('\n')}
              end
            end
        `;
        
        if (locale === 'en') {
            fs.writeFileSync(defaultMessages, code);
        } else {
            fs.writeFileSync(path.join(directory, `${locale}.rb`), code);
        }
        
    }
};
