import fs from 'fs';
import fsExtra from 'fs-extra';
import path from 'path';

import { interpolatify } from '../../utilities.js';

export function python(options) {
    const directory = path.join(options.repoPath, 'enolib');
    const defaultMessages = path.join(directory, 'messages.py');
    
    const messageFunction = message => {
        if (message.arguments) {
            return `${message.name} = lambda ${message.arguments.join(', ')}: f"${message.translation}"`;
        } else {
            return `${message.name} = '${message.translation.replace(/'/g, "\\'")}'`;
        }
    };
    
    for (const [locale, messages] of Object.entries(options.locales)) {
        const titleCaseLocale = locale.replace(/^./, initial => initial.toUpperCase());
        
        const code = interpolatify`
            # ${options.notice}
            
            ${messages.map(messageFunction).join('\n')}
        `;
        
        if (locale === 'en') {
            fs.writeFileSync(defaultMessages, code);
        } else {
            fs.writeFileSync(path.join(directory, `${locale}.py`), code);
        }
    }
};
