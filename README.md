# enolib-common

Data blueprints and scripts for generating message and specs code used across
enolib implementations in different programming languages.

## messages

Replace `../enolib-js` with the relative local path to some enolib
implementation folder that you want to generate messages for.

```
npm run messages -- ../enolib-js
```

## specs

Replace `../enolib-js` with the relative local path to some enolib
implementation folder that you want to generate specs for.

```
npm run specs -- ../enolib-js
```