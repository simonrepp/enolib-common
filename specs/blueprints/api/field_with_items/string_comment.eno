# Querying an existing, single-line, required string comment from a field with items

-- input
> comment
field:
- item
-- input

-- javascript
const output = parse(input).field('field').requiredStringComment();
-- javascript

-- php
$output = Enolib\Parser::parse($input)->field('field')->requiredStringComment();
-- php

-- python
output = enolib.parse(input).field('field').required_string_comment()
-- python

-- ruby
output = Enolib.parse(input).field('field').required_string_comment
-- ruby

## Result

-- string
comment
-- string

# Querying an existing, two-line, required string comment from a field with items

-- input
>comment
>  comment
field:
- item
-- input

-- javascript
const output = parse(input).field('field').requiredStringComment();
-- javascript

-- php
$output = Enolib\Parser::parse($input)->field('field')->requiredStringComment();
-- php

-- python
output = enolib.parse(input).field('field').required_string_comment()
-- python

-- ruby
output = Enolib.parse(input).field('field').required_string_comment
-- ruby

## Result

-- string
comment
  comment
-- string

# Querying an existing, required string comment with blank lines from a field with items

-- input
>
>     comment
>
>   comment
>
> comment
>
field:
- item
-- input

-- javascript
const output = parse(input).field('field').requiredStringComment();
-- javascript

-- php
$output = Enolib\Parser::parse($input)->field('field')->requiredStringComment();
-- php

-- python
output = enolib.parse(input).field('field').required_string_comment()
-- python

-- ruby
output = Enolib.parse(input).field('field').required_string_comment
-- ruby

## Result

-- string
    comment

  comment

comment
-- string

# Querying an optional, existing string comment from a field with items

-- input
> comment
field:
- item
-- input

-- javascript
const output = parse(input).field('field').optionalStringComment();
-- javascript

-- php
$output = Enolib\Parser::parse($input)->field('field')->optionalStringComment();
-- php

-- python
output = enolib.parse(input).field('field').optional_string_comment()
-- python

-- ruby
output = Enolib.parse(input).field('field').optional_string_comment
-- ruby

## Result

-- string
comment
-- string

# Querying an optional, missing string comment from a field with items

-- input
field:
- item
-- input

-- javascript
const output = parse(input).field('field').optionalStringComment();
-- javascript

-- php
$output = Enolib\Parser::parse($input)->field('field')->optionalStringComment();
-- php

-- python
output = enolib.parse(input).field('field').optional_string_comment()
-- python

-- ruby
output = Enolib.parse(input).field('field').optional_string_comment
-- ruby

## Nothing
