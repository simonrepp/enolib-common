# Expecting a field but getting an empty section

-- input
# element
-- input

-- javascript
parse(input).field('element');
-- javascript

-- php
Enolib\Parser::parse($input)->field('element');
-- php

-- python
enolib.parse(input).field('element')
-- python

-- ruby
Enolib.parse(input).field('element')
-- ruby

## ValidationError

-- text
A field with the key 'element' was expected.
-- text

-- snippet
   Line | Content
 >    1 | # element
-- snippet

selection: [0,0] => [0,9]


# Expecting a field but getting a section with a field with a value and a field with two items

-- input
# element

field: value

field:
- item
- item
-- input

-- javascript
parse(input).field('element');
-- javascript

-- php
Enolib\Parser::parse($input)->field('element');
-- php

-- python
enolib.parse(input).field('element')
-- python

-- ruby
Enolib.parse(input).field('element')
-- ruby

## ValidationError

-- text
A field with the key 'element' was expected.
-- text

-- snippet
   Line | Content
 >    1 | # element
 *    2 | 
 *    3 | field: value
 *    4 | 
 *    5 | field:
 *    6 | - item
 *    7 | - item
-- snippet

selection: [0,0] => [6,6]

# Expecting a field but getting a section with subsections

-- input
# section

## subsection

field: value

## subsection

field:
- item
- item
-- input

-- javascript
parse(input).field('section');
-- javascript

-- php
Enolib\Parser::parse($input)->field('section');
-- php

-- python
enolib.parse(input).field('section')
-- python

-- ruby
Enolib.parse(input).field('section')
-- ruby

## ValidationError

-- text
A field with the key 'section' was expected.
-- text

-- snippet
   Line | Content
 >    1 | # section
 *    2 | 
 *    3 | ## subsection
 *    4 | 
 *    5 | field: value
 *    6 | 
 *    7 | ## subsection
 *    8 | 
 *    9 | field:
 *   10 | - item
 *   11 | - item
-- snippet

selection: [0,0] => [10,6]
