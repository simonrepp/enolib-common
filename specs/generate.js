import { source } from './source.js';
import { javascript } from './generators/javascript.js';
import { php } from './generators/php.js';
import { python } from './generators/python.js';
import { ruby } from './generators/ruby.js';

export function specs(options) {
    options.specs = source();
    
    switch (options.language) {
        case 'js': javascript(options); break;
        case 'py': python(options); break;
        case 'php': php(options); break;
        case 'rb': ruby(options); break;
    }    
}
